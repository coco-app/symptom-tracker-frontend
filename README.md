# Coco - Your Quarantine Companion

This is the frontend of a prototype implemented during the EUvsVirus hackathon (24 -26 Apr 2020).
_Coco - Your Quarantine Companion_ reduces the manual communication burden between health authorities and people infected or exposed to COVID-19 who need to be in quarantine.

## Development

### Prerequities
* nodejs and npm
* angular cli: `npm i -g @angular/cli`

### Local Development
* checkout the repo
* `npm i`
* `ng serve`
