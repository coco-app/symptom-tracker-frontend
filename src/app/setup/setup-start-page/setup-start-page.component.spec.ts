import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupStartPageComponent } from './setup-start-page.component';

describe('SetupStartPageComponent', () => {
  let component: SetupStartPageComponent;
  let fixture: ComponentFixture<SetupStartPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetupStartPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupStartPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
