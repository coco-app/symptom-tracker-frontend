import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserDataService } from 'src/app/services/user-data.service';

@Component({
  selector: 'app-setup-start-page',
  templateUrl: './setup-start-page.component.html',
  styleUrls: ['./setup-start-page.component.scss']
})
export class SetupStartPageComponent {

  constructor(
    private router: Router,
    private userdata: UserDataService
  ) { }

  isTested() {
    this.userdata.setWasTested(true)
    this.router.navigate(['setup/tested'])
  }

  isNotTested() {
    this.userdata.setWasTested(false)
    this.router.navigate(['setup/possible-infection'])
  }


}
