import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Patient } from 'src/app/patient';
import { RegisterResponse } from 'src/app/register-response';
import { BackendService } from 'src/app/services/backend.service';
import { LocalStorageService, STORAGE_KEY } from 'src/app/services/local-storage.service';
import { UserDataService } from 'src/app/services/user-data.service';
import { Authority } from 'src/app/authority';

@Component({
  selector: 'app-autorithy-code-page',
  templateUrl: './autorithy-code-page.component.html',
  styleUrls: ['./autorithy-code-page.component.scss']
})
export class AutorithyCodePageComponent {

  authorityCode = 43950

  constructor(
    private router: Router,
    private localStorageService: LocalStorageService,
    private backend: BackendService,
    private userdata: UserDataService
  ) { }

  onSubmit() {
    // this.registerPatient();
    this.getHealthAuthority()
  }

  getHealthAuthority() {
    this.localStorageService.set(STORAGE_KEY.HEALTH_AUTHORITY_CODE, this.authorityCode)

    this.backend.getHealthAuthority(this.authorityCode.toString())
      .then((res: Authority) => {
        this.userdata.setHealthAuthority(res.name)
        this.router.navigate(['setup/completed']);
      }, (error) => {
        console.log(error)
      })
  }
}
