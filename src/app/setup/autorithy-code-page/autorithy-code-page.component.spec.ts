import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorithyCodePageComponent } from './autorithy-code-page.component';

describe('AutorithyCodePageComponent', () => {
  let component: AutorithyCodePageComponent;
  let fixture: ComponentFixture<AutorithyCodePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutorithyCodePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorithyCodePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
