import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestDatePageComponent } from './test-date-page.component';

describe('TestDatePageComponent', () => {
  let component: TestDatePageComponent;
  let fixture: ComponentFixture<TestDatePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestDatePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestDatePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
