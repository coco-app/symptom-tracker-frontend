import { Component, OnInit, Inject, LOCALE_ID } from '@angular/core';
import { Router } from '@angular/router';
import { UserDataService } from 'src/app/services/user-data.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-test-date-page',
  templateUrl: './test-date-page.component.html',
  styleUrls: ['./test-date-page.component.scss']
})
export class TestDatePageComponent {

  covidTestDate: string

  constructor(
    @Inject(LOCALE_ID) public locale: string,
    private router: Router,
    private userdata: UserDataService
  ) { 
    this.covidTestDate = formatDate(new Date(), 'yyyy-MM-dd', this.locale)
  }

  onSubmit() {
    this.userdata.setCovidTestDate(this.covidTestDate)
    this.router.navigate(['setup/authority-code'])
  }
}
