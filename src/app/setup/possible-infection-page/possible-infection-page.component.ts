import { Component, OnInit, Inject, LOCALE_ID } from '@angular/core';
import { Router } from '@angular/router';
import { UserDataService } from 'src/app/services/user-data.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-possible-infection-page',
  templateUrl: './possible-infection-page.component.html',
  styleUrls: ['./possible-infection-page.component.scss']
})
export class PossibleInfectionPageComponent implements OnInit {

  date: string

  constructor(
    @Inject(LOCALE_ID) public locale: string,
    private router: Router,
    private userdata: UserDataService
  ) {
    
  }

  ngOnInit(): void {
    this.date = formatDate(new Date(), 'yyyy-MM-dd', this.locale)
  }

  onSubmit() {
    this.userdata.setLastContactDate(this.date)
    this.router.navigate(['setup/authority-code'])
  }

}
