import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PossibleInfectionPageComponent } from './possible-infection-page.component';

describe('PossibleInfectionPageComponent', () => {
  let component: PossibleInfectionPageComponent;
  let fixture: ComponentFixture<PossibleInfectionPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PossibleInfectionPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PossibleInfectionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
