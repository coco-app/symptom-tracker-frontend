import { Component, OnInit } from '@angular/core';
import { UserDataService } from '../../services/user-data.service';
import { Router } from '@angular/router';
import { STORAGE_KEY, LocalStorageService } from 'src/app/services/local-storage.service';
import { Patient } from 'src/app/patient';
import { BackendService } from 'src/app/services/backend.service';
import { RegisterResponse } from 'src/app/register-response';

@Component({
  selector: 'app-setup-completed',
  templateUrl: './setup-completed.component.html',
  styleUrls: ['./setup-completed.component.scss']
})
export class SetupCompletedComponent implements OnInit {

  userName = ''
  healthAuthority

  constructor(
    private router: Router,
    private userData: UserDataService,
    private localStorageService: LocalStorageService,
    private backend: BackendService
  ) { }

  ngOnInit() {
    this.healthAuthority = this.userData.healthAuthority
  }

  startApp() {
    this.userData.setName(this.userName)
    this.registerPatient()
  }

  private registerPatient() {
    let authorityCode = this.localStorageService.get(STORAGE_KEY.HEALTH_AUTHORITY_CODE);

    let patient = new Patient();
    patient.health_authority_code = authorityCode;
    patient.name = this.userData.userName;
    
    this.backend.registerPatient(patient)
      .then((res: RegisterResponse) => {
        this.userData.setUserId(res.id);
        this.router.navigate(['/']);
      }, (error) => {
        console.log('could not register patient');
      });
  }
}
