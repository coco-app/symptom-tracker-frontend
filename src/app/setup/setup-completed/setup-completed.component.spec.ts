import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupCompletedComponent } from './setup-completed.component';

describe('SetupCompletedComponent', () => {
  let component: SetupCompletedComponent;
  let fixture: ComponentFixture<SetupCompletedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetupCompletedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupCompletedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
