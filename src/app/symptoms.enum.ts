export enum SYMPTOMS {
    FEVER = 0,
    TIREDNESS = 1,
    DRY_COUGH = 2,
    SHORTNESS_OF_BREATH = 3,
    ACHES = 4,
    SORE_THROAT = 5,
    DIARRHOEA = 6,
    NAUSEA = 7,
    RUNNY_NOSE = 8
}
