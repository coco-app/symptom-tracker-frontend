import { formatDate } from '@angular/common';
import { Component, EventEmitter, Inject, LOCALE_ID, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-symptoms-calendar',
  templateUrl: './symptoms-calendar.component.html',
  styleUrls: ['./symptoms-calendar.component.scss']
})
export class SymptomsCalendarComponent implements OnInit {

  @Output() dateChanged = new EventEmitter();

  date: string
  dateIndex: number
  todayIndex: number
  startDate: Date

  constructor(
    @Inject(LOCALE_ID) public locale: string,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.date = params.get('date')
    })

    this.startDate = this.calculateCalendarStart()
    this.dateIndex = this.getDateIndex(this.date)
    this.todayIndex = this.getDateIndex(new Date())

    this.dateChanged.emit(formatDate(this.getDate(this.dateIndex), 'yyyy-MM-dd', this.locale))
  }

  isSelected(index) {
    return this.dateIndex === index
  }

  isDisabled(index) {
    return index > this.todayIndex
  }

  getDayNumber(index) {
    const date = new Date(this.startDate)
    date.setDate(date.getDate() + index)
    return date.getDate()
  }

  onDateSelected(index) {
    const selectedDate = this.getDate(index)
    this.dateIndex = this.getDateIndex(selectedDate)
    this.router.navigate(['/symptoms', formatDate(selectedDate, 'yyyy-MM-dd', this.locale)])
    this.dateChanged.emit(formatDate(selectedDate, 'yyyy-MM-dd', this.locale))
  }

  private getDate(index) {
    const date = new Date(this.startDate)
    date.setDate(date.getDate() + index)
    return date
  }

  private getDateIndex(date) {
    const selectedDate = new Date(date)
    selectedDate.setHours(0)
    selectedDate.setMinutes(0)
    selectedDate.setSeconds(0)
    selectedDate.setMilliseconds(0)
    const startDate = new Date(this.startDate)
    startDate.setHours(0)
    startDate.setMinutes(0)
    startDate.setSeconds(0)
    startDate.setMilliseconds(0)

    let diff = selectedDate.getTime() - startDate.getTime()
    return diff / (24 * 60 * 60 * 1000)
  }

  private calculateCalendarStart() {
    const startDate = new Date()

    let diffWeekDay = startDate.getDay() - 1
    if (diffWeekDay < 0) {
      diffWeekDay = 6
    }

    startDate.setDate(startDate.getDate() - 7 - diffWeekDay)
    return startDate
  }

}
