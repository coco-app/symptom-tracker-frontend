import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SymptomsCalendarComponent } from './symptoms-calendar.component';

describe('SymptomsCalendarComponent', () => {
  let component: SymptomsCalendarComponent;
  let fixture: ComponentFixture<SymptomsCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SymptomsCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SymptomsCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
