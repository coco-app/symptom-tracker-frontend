export interface Authority {
    name: string
    code: string
}
