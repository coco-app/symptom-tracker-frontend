import { Injectable } from '@angular/core';
import { LocalStorageService, STORAGE_KEY } from './local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  userId: string
  userName: string
  healthAuthority: string
  symptomFreeSince: string
  covidTestDate: string
  lastContactDate: string
  wasTested: boolean

  constructor(private localStorageService: LocalStorageService) { }

  init() {
    this.userName = this.localStorageService.get(STORAGE_KEY.NAME)
    this.userId = this.localStorageService.get(STORAGE_KEY.USERID)
    this.healthAuthority = this.localStorageService.get(STORAGE_KEY.HEALTH_AUTHORITY)
    this.symptomFreeSince = this.localStorageService.get(STORAGE_KEY.SYMPTOM_FREE_SINCE)
    this.covidTestDate = this.localStorageService.get(STORAGE_KEY.COVID_TEST_DATE)
    this.lastContactDate = this.localStorageService.get(STORAGE_KEY.LAST_CONTACT_DATE)
    this.wasTested = this.localStorageService.get(STORAGE_KEY.WAS_TESTED) == true
  }

  getInfectionDate() {
    if (this.wasTested) {
      return this.covidTestDate
    } else {
      return this.lastContactDate
    }
  }

  setUserId(id: string) {
    this.userId = id
    this.localStorageService.set(STORAGE_KEY.USERID, id)
  }

  setName(name: string) {
    this.userName = name
    this.localStorageService.set(STORAGE_KEY.NAME, name)
    // TODO: post user to backend
  }

  setHealthAuthority(name: string) {
    this.healthAuthority = name
    this.localStorageService.set(STORAGE_KEY.HEALTH_AUTHORITY, name)
  }

  setSymptomFreeSince(dateString: string) {
    this.symptomFreeSince = dateString
    this.localStorageService.set(STORAGE_KEY.SYMPTOM_FREE_SINCE, dateString)
  }

  setCovidTestDate(dateString: string) {
    this.covidTestDate = dateString
    this.localStorageService.set(STORAGE_KEY.COVID_TEST_DATE, dateString)
  }

  setLastContactDate(dateString: string) {
    this.lastContactDate = dateString
    this.localStorageService.set(STORAGE_KEY.LAST_CONTACT_DATE, dateString)
  }

  setWasTested(wasTested: boolean) {
    this.wasTested = wasTested
    this.localStorageService.set(STORAGE_KEY.WAS_TESTED, wasTested)
  }
}
