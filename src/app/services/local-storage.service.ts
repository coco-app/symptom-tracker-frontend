import { Injectable } from '@angular/core';

export const STORAGE_KEY = {
  NAME: "name",
  USERID: "userid",
  HEALTH_AUTHORITY: "health_authority",
  HEALTH_AUTHORITY_CODE: "health_authority_code",
  INFECTED_SICNE: "infected_since",
  SYMPTOMS: "symptoms_",
  SYMPTOM_FREE_SINCE: "symptom_free_since",
  COVID_TEST_DATE: "covid_test_date",
  WAS_TESTED: 'was_tested',
  LAST_CONTACT_DATE: "last_contact_date"
}

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  set(key: string, data: any): void {
    try {
      localStorage.setItem(key, JSON.stringify(data));
    } catch (e) {
      console.error('Error saving to localStorage', e);
    }
  }

  get(key: string) {
    try {
      return JSON.parse(localStorage.getItem(key));
    } catch (e) {
      console.error('Error getting data from localStorage', e);
      return null;
    }
  }

}
