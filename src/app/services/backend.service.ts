import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Patient } from '../patient';
import { PatientStatus } from '../patient-status';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  // private backendUrl = 'http://localhost:8080'
  private backendUrl = 'https://api.coco-app.eu'

  constructor(private http: HttpClient) { }

  registerPatient(patient: Patient) {
    let url = this.backendUrl + '/patient'
    console.log('POST to ' + url)
    return this.http.post(url, patient).toPromise()
  }

  updatePatientStatus(id: string, status: PatientStatus) {
    let url = this.backendUrl + '/patient/' + id
    console.log('PUT to ' + url)
    return this.http.put(url, status).toPromise()
  }

  getHealthAuthority(id: string) {
    let url = this.backendUrl + '/health-authority/' + id
    console.log('GET to ' + url)
    return this.http.get(url).toPromise()
  }
}
