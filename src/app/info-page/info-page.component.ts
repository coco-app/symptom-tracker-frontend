import { Component, OnInit } from '@angular/core';
import { TABS } from '../tabs.enum';

@Component({
  selector: 'app-info-page',
  templateUrl: './info-page.component.html',
  styleUrls: ['./info-page.component.scss']
})
export class InfoPageComponent implements OnInit {

  tabKey: TABS

  constructor() { }

  ngOnInit(): void {
    this.tabKey = TABS.INFO
  }

}
