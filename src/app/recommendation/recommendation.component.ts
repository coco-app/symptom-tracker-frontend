import { Component, OnInit } from '@angular/core';
import { UserDataService } from '../services/user-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-recommendation',
  templateUrl: './recommendation.component.html',
  styleUrls: ['./recommendation.component.scss']
})
export class RecommendationComponent implements OnInit {

  daysLeft: number

  constructor(
    private userdata: UserDataService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.daysLeft = this.calculateDaysLeft()

    if (this.daysLeft == 0) {
      this.router.navigate(['end'])
    }
  }

  calculateDaysLeft(): number {
    const today = new Date()
    let date: Date
    let daysLeft: number

    // get correct date to start with
    if (this.userdata.wasTested) {
      date = new Date(this.userdata.covidTestDate)
    } else {
      date = new Date(this.userdata.lastContactDate)
    }

    // calculate regular days left
    daysLeft = this.getDifferenceInDays(date, today) + 14

    // if days left < 2, check for symptom free
    if (daysLeft < 2) {
      if (this.userdata.symptomFreeSince == null) {
        // not sympton free
        daysLeft = 2
      } else {
        // check if symptom free for more than 2 days
        const symptomFree = new Date(this.userdata.symptomFreeSince)
        const symptomFreeFor = this.getDifferenceInDays(today, symptomFree)

        if (symptomFreeFor < 2) {
          // if not symptom free for 2 or more days, show 2 days left
          daysLeft = 2
        }
      }
    }

    // ensure no negative days
    if (daysLeft < 0) {
      daysLeft = 0
    }

    return daysLeft
  }

  getDifferenceInDays(first: Date, second: Date): number {
    first.setHours(0)
    first.setMinutes(0)
    first.setSeconds(0)
    first.setMilliseconds(0)

    second.setHours(0)
    second.setMinutes(0)
    second.setSeconds(0)
    second.setMilliseconds(0)

    const firstMillis = first.getTime()
    const secondMillis = second.getTime()

    const days = (firstMillis - secondMillis) / (1000 * 60 * 60 * 24)
    
    return days
  }
}
