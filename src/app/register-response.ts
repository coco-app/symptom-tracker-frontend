import { Authority } from './authority';

export interface RegisterResponse {
    id: string
    name: string
    health_authority: Authority
}
