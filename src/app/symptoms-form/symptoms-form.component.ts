import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PatientStatus } from '../patient-status';
import { BackendService } from '../services/backend.service';
import { LocalStorageService, STORAGE_KEY } from '../services/local-storage.service';
import { UserDataService } from '../services/user-data.service';
import { SYMPTOMS } from '../symptoms.enum';

@Component({
  selector: 'app-symptoms-form',
  templateUrl: './symptoms-form.component.html',
  styleUrls: ['./symptoms-form.component.scss']
})
export class SymptomsFormComponent implements OnInit {

  @Input() date: string
  @Output() symptomsChanged = new EventEmitter()

  SYMPTOMS = SYMPTOMS
  symptomKeys: string[]

  symptoms = []

  constructor(
    private userData: UserDataService,
    private localStorageService: LocalStorageService,
    private backend: BackendService
  ) { }

  ngOnInit(): void {
    // initialize keys of symptoms enum
    let keys = Object.keys(SYMPTOMS)
    this.symptomKeys = keys.slice(keys.length / 2)

    this.initSymptoms()
  }

  ngOnChanges() {
    this.initSymptoms()
  }

  symptomToString(symptomCode) {
    switch (symptomCode) {
      case SYMPTOMS.ACHES:
        return "aches"
      case SYMPTOMS.DIARRHOEA:
        return "diarrhoea"
      case SYMPTOMS.DRY_COUGH:
        return "dry cough"
      case SYMPTOMS.FEVER:
        return "fever"
      case SYMPTOMS.NAUSEA:
        return "nausea"
      case SYMPTOMS.RUNNY_NOSE:
        return "runny nose"
      case SYMPTOMS.SHORTNESS_OF_BREATH:
        return "shortness of breath"
      case SYMPTOMS.SORE_THROAT:
        return "throat"
      case SYMPTOMS.TIREDNESS:
        return "tiredness"
    }
  }

  isSymptomSelected(symptomCode) {
    return this.symptoms.indexOf(symptomCode) !== -1
  }

  onSelectSymptom(symptomCode) {
    if (this.isSymptomSelected(symptomCode)) {
      let index = this.symptoms.indexOf(symptomCode)
      this.symptoms.splice(index, 1)
    } else {
      this.symptoms.push(symptomCode)
    }

    this.saveSymptoms()
    this.symptomsChanged.emit()
  }

  private initSymptoms() {
    // initialize symptoms
    let data = this.localStorageService.get(STORAGE_KEY.SYMPTOMS + this.date)
    if (data) {
      this.symptoms = data.symptoms
    } else {
      this.symptoms = []
    }
  }

  private saveSymptoms() {
    this.localStorageService.set(STORAGE_KEY.SYMPTOMS + this.date, { date: this.date, symptoms: this.symptoms })

    // update symptom_free_since
    if (this.userData.symptomFreeSince == null && !this.hasSymptoms()) {
      // symptom free since this entry
      this.userData.setSymptomFreeSince(this.date)

      this.sendStatus(this.date)

    } else if (this.userData.symptomFreeSince != null && this.hasSymptoms()) {
      // there are symptoms again
      this.userData.setSymptomFreeSince(null)

      this.sendStatus(null)
    }
  }

  private sendStatus(symptom_free_since?: string) {
    let id = this.userData.userId

    let status = new PatientStatus()
    status.infected_since = this.userData.symptomFreeSince
    status.symptom_free_since = symptom_free_since

    this.backend.updatePatientStatus(id, status)
      .then((res) => {
        console.log('patient status submitted')
      }, (error) => {
        console.log(error)
      })
  }

  private hasSymptoms() {
    return this.symptoms.length !== 0
  }

}
