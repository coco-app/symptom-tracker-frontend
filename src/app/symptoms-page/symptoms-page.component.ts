import { formatDate } from '@angular/common';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { LocalStorageService, STORAGE_KEY } from '../services/local-storage.service';
import { UserDataService } from '../services/user-data.service';
import { TABS } from '../tabs.enum';

@Component({
  selector: 'app-symptoms-page',
  templateUrl: './symptoms-page.component.html',
  styleUrls: ['./symptoms-page.component.scss']
})
export class SymptomsPageComponent implements OnInit {

  tabKey: TABS

  date: string

  symptomsCount: number[]
  timeSpan = 0
  infectionDate: Date

  constructor(
    @Inject(LOCALE_ID) public locale: string,
    private userData: UserDataService,
    private localStorageService: LocalStorageService
  ) { }

  ngOnInit(): void {
    this.tabKey = TABS.SYMPTOMS
    this.initTimeSpanForCounts()
    this.onSymptomsChanged()
  }

  onDateChanged(newDate) {
    this.date = newDate
  }

  onSymptomsChanged() {
    this.symptomsCount = []
    for (let i = 0; i < this.timeSpan; i++) {
      let nextDate = new Date()
      nextDate.setDate(this.infectionDate.getDate() + i)
      let data = this.localStorageService.get(STORAGE_KEY.SYMPTOMS + formatDate(nextDate, 'yyyy-MM-dd', this.locale))
      if (data) {
        this.symptomsCount.push(data.symptoms.length)
      } else {
        this.symptomsCount.push(0)
      }
    }
  }

  private initTimeSpanForCounts() {
    const today = new Date()
    today.setHours(0)
    today.setMinutes(0)
    today.setSeconds(0)
    today.setMilliseconds(0)
    this.infectionDate = new Date(this.userData.getInfectionDate())
    let infectionDate = new Date(this.infectionDate)
    infectionDate.setHours(0)
    infectionDate.setMinutes(0)
    infectionDate.setSeconds(0)
    infectionDate.setMilliseconds(0)

    let diff = today.getTime() - infectionDate.getTime()
    this.timeSpan = diff / (24 * 60 * 60 * 1000) + 1
  }

}
