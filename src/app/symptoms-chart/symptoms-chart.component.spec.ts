import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SymptomsChartComponent } from './symptoms-chart.component';

describe('SymptomsChartComponent', () => {
  let component: SymptomsChartComponent;
  let fixture: ComponentFixture<SymptomsChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SymptomsChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SymptomsChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
