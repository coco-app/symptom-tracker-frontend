import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-symptoms-chart',
  templateUrl: './symptoms-chart.component.html',
  styleUrls: ['./symptoms-chart.component.scss']
})
export class SymptomsChartComponent implements OnInit {

  @Input() symptomNumbers: number[]

  type
  data
  options

  constructor() { }

  ngOnInit(): void {
    this.type = 'scatter'
    this.options = {
      legend: {
        display: false
      },
      scales: {
        yAxes: [{
          display: false,
          ticks: {
            max: 9,
            min: 1,
            stepSize: 1
          },
          stacked: true
        }],
        xAxes: [{
          type: 'category',
          labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14"],
          ticks: {
            max: 14,
            min: 1,
            stepSize: 1
          },
          gridLines: {
            display: false
          },
        }]
      },
      responsive: true,
      maintainAspectRatio: false
    }
  }

  ngOnChanges() {
    this.setData()
  }

  setData() {
    const datasets = []
    for (let i = 1; i < 9; i++) {
      const dataset = []
      for (let j in this.symptomNumbers) {
        const number = this.symptomNumbers[j]
        if (i <= number) {
          dataset.push({ x: +j + 1, y: +i + 1 })
        } else {
          dataset.push({ x: +j + 1, y: 0 })
        }
      }
      datasets.push({
        data: dataset,
        pointBorderColor: "rgb(39,71,130)",
        pointBackgroundColor: "rgb(39,71,130)"
      })
    }

    this.data = {
      labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14"],
      datasets: datasets
    }
  }

}
