export class PatientStatus {
    infected_since: string;
    symptom_free_since?: string;
}
