import { formatDate } from '@angular/common';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { UserDataService } from '../services/user-data.service';

@Component({
  selector: 'app-end-page',
  templateUrl: './end-page.component.html',
  styleUrls: ['./end-page.component.scss']
})
export class EndPageComponent implements OnInit {

  today: string
  userName = ''
  wasTested: boolean

  constructor(
    @Inject(LOCALE_ID) public locale: string,
    private userData: UserDataService
  ) { }

  ngOnInit(): void {
    this.userName = this.userData.userName
    this.wasTested = this.userData.wasTested
    let today = new Date()
    this.today = formatDate(today, 'yyyy-MM-dd', this.locale)
  }

}
