import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { EndPageComponent } from './end-page/end-page.component';
import { HomePageComponent } from './home-page/home-page.component';
import { InfoPageComponent } from './info-page/info-page.component';
import { AutorithyCodePageComponent } from './setup/autorithy-code-page/autorithy-code-page.component';
import { PossibleInfectionPageComponent } from './setup/possible-infection-page/possible-infection-page.component';
import { SetupCompletedComponent } from './setup/setup-completed/setup-completed.component';
import { SetupStartPageComponent } from './setup/setup-start-page/setup-start-page.component';
import { TestDatePageComponent } from './setup/test-date-page/test-date-page.component';
import { SymptomsPageComponent } from './symptoms-page/symptoms-page.component';


const routes: Routes = [
  { path: '', component: HomePageComponent, data: { animation: 'Home' } },
  { path: 'symptoms/:date', component: SymptomsPageComponent, data: { animation: 'Symptoms' } },
  { path: 'info', component: InfoPageComponent, data: { animation: 'Info' } },
  { path: 'setup', component: SetupStartPageComponent, data: { animation: 'SetupStart' } },
  { path: 'setup/possible-infection', component: PossibleInfectionPageComponent, data: { animation: 'SetupDate' } },
  { path: 'setup/tested', component: TestDatePageComponent, data: { animation: 'SetupDate' } },
  { path: 'setup/authority-code', component: AutorithyCodePageComponent, data: { animation: 'SetupCode' } },
  { path: 'setup/completed', component: SetupCompletedComponent, data: { animation: 'SetupEnd' } },
  { path: 'end', component: EndPageComponent }
];

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
