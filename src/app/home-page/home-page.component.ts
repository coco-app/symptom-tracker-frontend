import { Component, OnInit, Inject, LOCALE_ID, NgZone } from '@angular/core';
import { UserDataService } from '../services/user-data.service';
import { Observable } from 'rxjs';
import { PushNotificationService } from '../services/push-notification.service';
import { Router } from '@angular/router';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  userName = ''

  constructor(
    @Inject(LOCALE_ID) public locale: string,
    private userData: UserDataService,
    private notifications: PushNotificationService,
    private router: Router,
    private ngZone: NgZone
  ) { }

  ngOnInit(): void {
    this.userName = this.userData.userName

    setTimeout(() => {this.requestPermission()}, 0)
    setTimeout(() => {this.notify()}, 7 * 1000)
  }

  requestPermission() {
    this.notifications.requestPermission()
  }

  notify() {
    let options = {
      body: 'How are you feeling? Track your symptoms for today 🩺!',
      icon: '/assets/image/preloader.png'
    }

    this.notifications.create('Coco', options)
      .subscribe(
        (res) => {
          if (res.event.type === 'click') {
            this.ngZone.run(() => this.router.navigate(['/symptoms', formatDate(new Date(), 'yyyy-MM-dd', this.locale)]))
          }
        },
        (err) => console.log(err))
  }
}
