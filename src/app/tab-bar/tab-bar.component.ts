import { formatDate } from '@angular/common';
import { Component, Inject, Input, LOCALE_ID, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TABS } from '../tabs.enum';

@Component({
  selector: 'app-tab-bar',
  templateUrl: './tab-bar.component.html',
  styleUrls: ['./tab-bar.component.scss']
})
export class TabBarComponent implements OnInit {

  @Input() activeTab: TABS

  today: string

  constructor(
    @Inject(LOCALE_ID) public locale: string,
    private router: Router
  ) { }

  ngOnInit(): void {
    let today = new Date()
    this.today = formatDate(today, 'yyyy-MM-dd', this.locale)
  }

  isSymptomsActive() {
    return this.activeTab === TABS.SYMPTOMS
  }

  isInfoActive() {
    return this.activeTab === TABS.INFO
  }

  routeForSymptomTab() {
    if (this.isSymptomsActive()) {
      this.router.navigate(['/']);
    } else {
      this.router.navigate(['/symptoms', this.today]);
    }
  }

  routeForInfoTab() {
    if (this.isInfoActive()) {
      this.router.navigate(['/']);
    } else {
      this.router.navigate(['/info']);
    }
  }

}
