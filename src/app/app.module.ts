import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { ChartModule } from 'angular2-chartjs';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EndPageComponent } from './end-page/end-page.component';
import { HomePageComponent } from './home-page/home-page.component';
import { InfoPageComponent } from './info-page/info-page.component';
import { RecommendationComponent } from './recommendation/recommendation.component';
import { AutorithyCodePageComponent } from './setup/autorithy-code-page/autorithy-code-page.component';
import { PossibleInfectionPageComponent } from './setup/possible-infection-page/possible-infection-page.component';
import { SetupCompletedComponent } from './setup/setup-completed/setup-completed.component';
import { SetupStartPageComponent } from './setup/setup-start-page/setup-start-page.component';
import { TestDatePageComponent } from './setup/test-date-page/test-date-page.component';
import { SymptomsCalendarComponent } from './symptoms-calendar/symptoms-calendar.component';
import { SymptomsChartComponent } from './symptoms-chart/symptoms-chart.component';
import { SymptomsFormComponent } from './symptoms-form/symptoms-form.component';
import { SymptomsPageComponent } from './symptoms-page/symptoms-page.component';
import { TabBarComponent } from './tab-bar/tab-bar.component';


@NgModule({
  declarations: [
    AppComponent,
    RecommendationComponent,
    SymptomsPageComponent,
    HomePageComponent,
    SymptomsFormComponent,
    SetupCompletedComponent,
    InfoPageComponent,
    SetupStartPageComponent,
    PossibleInfectionPageComponent,
    AutorithyCodePageComponent,
    TestDatePageComponent,
    TabBarComponent,
    SymptomsCalendarComponent,
    SymptomsChartComponent,
    EndPageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ChartModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
